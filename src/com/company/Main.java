package com.company;

import com.company.Message.Message;
import com.company.Message.Request;
import com.company.Message.Response;
import com.company.ResponseChainOfResponsibility.BitmapResponseHandler;
import com.company.ResponseChainOfResponsibility.ResponseHandler;
import com.company.ResponseChainOfResponsibility.TextResponseHandler;
import org.w3c.dom.Text;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String args[]) throws Exception {
        TCPServer server = new TCPServer();
        ServerSocket serverSocket = new ServerSocket(6789);
        System.out.println("Uruchomiono serwer.");

        ResponseHandler textResponseHandler = new TextResponseHandler();
        ResponseHandler bitmapResponseHandler = new BitmapResponseHandler();
        textResponseHandler.setNextResponseHandler(bitmapResponseHandler);

        while (true) {
            Socket connectionSocket = serverSocket.accept();
            Request requestToSend = server.createAndGetRequest();

            OutputStream outputStream = connectionSocket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            //Tutaj też należy zrobić strategię wysyłania do jednego/kilku/wszystkich
            System.out.println("Wysyłanie...");
            objectOutputStream.writeObject(requestToSend);

            //Tutaj musi być odbiór odpowiedzi i deserializacja
            System.out.println("Oczekiwanie na odpowiedź...");
            try {
                InputStream inputStream = connectionSocket.getInputStream();
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                Response response = (Response)objectInputStream.readObject();
                textResponseHandler.handleResponse(response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
