package com.company.Message;
import java.io.Serializable;

public abstract class Message implements Serializable {
    private static final long serialVersionUID = 1L;
    protected MessageEnum requestCode;
    private byte[] content;
    private Message next;

    public void setContent(byte[] content) {
        this.content = content;
    }

    public byte[] getContent() {
        return content;
    }

    public void setRequestCode(MessageEnum requestCode) {
        this.requestCode = requestCode;
    }

    public MessageEnum getRequestCode() {
        return this.requestCode;
    }
}
