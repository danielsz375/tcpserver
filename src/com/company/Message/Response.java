package com.company.Message;

public class Response extends Message {
    private static final long serialVersionUID = 1L;
    public Response(MessageEnum requestCode, byte[] content) {
        this.requestCode = requestCode;
        this.setContent(content);
    }
}