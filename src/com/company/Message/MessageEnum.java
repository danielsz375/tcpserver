package com.company.Message;

public enum MessageEnum {
    REQUEST_TXT,
    REQUEST_BMP,
    RESPONSE_TXT,
    RESPONSE_BMP
}
