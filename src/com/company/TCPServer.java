package com.company;

import com.company.Message.MessageEnum;
import com.company.Message.Request;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class TCPServer {

    public Request createAndGetRequest() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Wybierz opcję:");
            System.out.println("[1] - TEXT REQUEST");
            System.out.println("[2] - BMP REQUEST");
            int option = scanner.nextInt();
            switch(option) {
                case 1: return new Request(MessageEnum.REQUEST_TXT);
                case 2: return new Request(MessageEnum.REQUEST_BMP);
                default: throw new IOException("Niewłaściwa opcja");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        // Nie wiem co zrobić w tej sytuacji
        return null;
    }


}
