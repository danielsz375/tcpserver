package com.company.ResponseChainOfResponsibility;

import com.company.Message.MessageEnum;
import com.company.Message.Response;
import org.w3c.dom.ls.LSOutput;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class BitmapResponseHandler extends ResponseHandler {
    @Override
    public void handleResponse(Response response) {
        if(response.getRequestCode() == MessageEnum.RESPONSE_BMP) {
            try {
                String pathname = "C:\\screenshot.bmp";
                ByteArrayInputStream input_stream= new ByteArrayInputStream(response.getContent());
                BufferedImage final_buffered_image = ImageIO.read(input_stream);
                ImageIO.write(final_buffered_image , "bmp", new File(pathname) );
                System.out.println("Screenshot został zapisany w lokalizacji: " + pathname);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            this.getNextResponseHandler().handleResponse(response);
        }
    }
}
