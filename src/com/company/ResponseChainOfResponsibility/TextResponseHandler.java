package com.company.ResponseChainOfResponsibility;

import com.company.Message.MessageEnum;
import com.company.Message.Response;

public class TextResponseHandler extends ResponseHandler {
    @Override
    public void handleResponse(Response response) {
        if(response.getRequestCode() == MessageEnum.RESPONSE_TXT) {
            System.out.println("Odczytany tekst:");
            String decodedResponseContent = new String(response.getContent());
            System.out.println(decodedResponseContent);
        } else {
            this.getNextResponseHandler().handleResponse(response);
        }
    }
}
