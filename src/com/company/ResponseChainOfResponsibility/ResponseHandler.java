package com.company.ResponseChainOfResponsibility;

import com.company.Message.Response;

public abstract class ResponseHandler {
    private ResponseHandler nextResponseHandler;

    public void setNextResponseHandler(ResponseHandler nextResponseHandler) {
        this.nextResponseHandler = nextResponseHandler;
    }

    public abstract void handleResponse(Response response);

    public ResponseHandler getNextResponseHandler() {
        return this.nextResponseHandler;
    }
}
